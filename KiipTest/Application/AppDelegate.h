//
//  AppDelegate.h
//  App
//
//  Created by Mihaela Mihaljević Jakić on 03/02/15.
//  Copyright (c) 2015 Mihaela Mihaljević Jakić. All rights reserved.
//

#import <UIKit/UIKit.h>

//Kiip
//Kiip framework
#import <KiipSDK/KiipSDK.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate, KiipDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (nonatomic, strong) Kiip *kiip;

@end

