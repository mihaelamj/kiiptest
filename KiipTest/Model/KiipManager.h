//
//  KiipManager.h
//  KiipTest
//
//  Created by Mihaela Mihaljević Jakić on 10/02/15.
//  Copyright (c) 2015 Purch. All rights reserved.
//

#import <Foundation/Foundation.h>

//Kiip framework
#import <KiipSDK/KiipSDK.h>

@interface KiipManager : NSObject

+ (Kiip *)sharedKiipManager;

@end
