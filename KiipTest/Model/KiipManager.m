//
//  KiipManager.m
//  KiipTest
//
//  Created by Mihaela Mihaljević Jakić on 10/02/15.
//  Copyright (c) 2015 Purch. All rights reserved.
//

#import "KiipManager.h"

//Kiip consts
//#import "KiipCustomConsts.h"

@implementation KiipManager

#pragma mark -
#pragma mark Singleton

+ (Kiip *)sharedKiipManager
{
    static Kiip *_kiipManager = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        _kiipManager = [[Kiip alloc] initWithAppKey:@"ca596ec5111a6ab4b7fc9eba34859014" andSecret:@"aca35562dbb427729537199db970c72c"];
        [Kiip setSharedInstance:_kiipManager];
    });
    
    return _kiipManager;
}

@end
